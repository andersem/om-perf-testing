package com.abbsnabb.resources;

import com.abbsnabb.domain.FooBar;
import com.abbsnabb.domain.SkattekortResponse;
import com.abbsnabb.domain.SomethingElse;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.io.IOUtils;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.GZIPOutputStream;

@Path("performance")
public class PerformanceTestResource {

    private final SettableGauge writeAsStreamGauge;
    private final SettableGauge writeAsStringGauge;
    private final SettableGauge writeAsStringFromStreamGauge;
    private MetricRegistry metricRegistry;

    public PerformanceTestResource(MetricRegistry metricRegistry) {
        this.metricRegistry = metricRegistry;

        writeAsStreamGauge = new SettableGauge();
        metricRegistry.register(MetricRegistry.name(
                PerformanceTestResource.class, "writeAsStream", "foobarCount"), writeAsStreamGauge);

        writeAsStringGauge = new SettableGauge();
        metricRegistry.register(MetricRegistry.name(
                PerformanceTestResource.class, "writeAsString", "foobarCount"), writeAsStringGauge);

        writeAsStringFromStreamGauge = new SettableGauge();
        metricRegistry.register(MetricRegistry.name(
                PerformanceTestResource.class, "writeAsStringFromStream", "foobarCount"),
                writeAsStringFromStreamGauge);
    }

    @Timed
    @GET
    @Path("writeAsString")
    public Response writeAsString(
            @QueryParam("foobarCount") @DefaultValue("300000") Integer foobarCount) throws JsonProcessingException {
        writeAsStringGauge.setValue(foobarCount);
        SkattekortResponse skattekortResponse = generateSkattekortResponse(foobarCount);
        String response = new ObjectMapper().writeValueAsString(skattekortResponse);
        writeAsStringGauge.setValue(0);
        return Response.ok()
                .entity(response)
                .build();
    }

    @Timed
    @GET
    @Path("writeAsStream")
    public Response writeAsStream(
            @QueryParam("foobarCount") @DefaultValue("300000") Integer foobarCount) throws IOException {
        writeAsStreamGauge.setValue(foobarCount);
        SkattekortResponse skattekortResponse = generateSkattekortResponse(foobarCount);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        new ObjectMapper().writeValue(new GZIPOutputStream(new Base64OutputStream(out)),
                skattekortResponse);

        writeAsStreamGauge.setValue(0);
        return Response.ok()
                .entity(out.toString())
                .build();
    }

    @Timed
    @GET
    @Path("writeAsStringFromStream")
    public Response writeAsStringFromStream(
            @QueryParam("foobarCount") @DefaultValue("300000") Integer foobarCount) throws IOException {
        writeAsStringFromStreamGauge.setValue(foobarCount);
        SkattekortResponse skattekortResponse = generateSkattekortResponse(foobarCount);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        new ObjectMapper().writeValue(out, skattekortResponse);

        writeAsStringFromStreamGauge.setValue(0);
        return Response.ok()
                .entity(out.toString())
                .build();
    }

    private SkattekortResponse generateSkattekortResponse(int foobarCount) {
        List<FooBar> foobars1 = generateFoobar(foobarCount);
        List<FooBar> foobars2 = generateFoobar(foobarCount);
        return new SkattekortResponse(foobars1, foobars2);
    }

    private List<FooBar> generateFoobar(int foobarCount) {
        return IntStream.range(0, foobarCount)
                .mapToObj(i -> new FooBar(UUID.randomUUID()
                        .toString(), UUID.randomUUID()
                        .toString(), IntStream.range(0, 4)
                        .mapToObj(j -> new SomethingElse())
                        .collect(Collectors.toList())))
                .collect(Collectors.toList());
    }

    public static class SettableGauge implements Gauge {
        private Integer value = 0;

        public void setValue(Integer value) {
            this.value = value;
        }

        @Override
        public Integer getValue() {
            return value;
        }
    }

}
