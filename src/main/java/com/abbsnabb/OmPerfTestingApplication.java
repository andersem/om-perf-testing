package com.abbsnabb;

import com.abbsnabb.resources.PerformanceTestResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.dropwizard.DropwizardExports;
import io.prometheus.client.exporter.MetricsServlet;

public class OmPerfTestingApplication extends Application<OmPerfTestingConfiguration> {

    public static void main(final String[] args) throws Exception {
        new OmPerfTestingApplication().run(args);
    }

    @Override
    public String getName() {
        return "omPerfTesting";
    }

    @Override
    public void initialize(final Bootstrap<OmPerfTestingConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final OmPerfTestingConfiguration configuration,
                    final Environment environment) {
        environment.jersey().register(new PerformanceTestResource(environment.metrics()));

        CollectorRegistry.defaultRegistry.register(new DropwizardExports(environment.metrics()));

        environment.servlets().addServlet("metrics", new MetricsServlet()).addMapping("/metrics");
    }

}
