package com.abbsnabb.domain;

import java.util.UUID;

public class SomethingElse {
    public final String anything;
    public final String other;
    public final String than;
    public final String thus;
    public final String is;
    public final String too;
    public final String much;

    public SomethingElse(
            String anything,
            String other,
            String than,
            String thus,
            String is,
            String too,
            String much) {
        this.anything = anything;
        this.other = other;
        this.than = than;
        this.thus = thus;
        this.is = is;
        this.too = too;
        this.much = much;
    }

    public SomethingElse() {
        this.anything = UUID.randomUUID().toString();
        this.other = UUID.randomUUID().toString();
        this.than = UUID.randomUUID().toString();
        this.thus = UUID.randomUUID().toString();
        this.is = UUID.randomUUID().toString();
        this.too = UUID.randomUUID().toString();
        this.much = UUID.randomUUID().toString();
    }
}
