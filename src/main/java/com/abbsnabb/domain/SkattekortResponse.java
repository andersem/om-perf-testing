package com.abbsnabb.domain;

import java.util.List;

public class SkattekortResponse {
    public final List<FooBar> fooBar1;
    public final List<FooBar> fooBar2;

    public SkattekortResponse(List<FooBar> fooBar1, List<FooBar> fooBar2) {
        this.fooBar1 = fooBar1;
        this.fooBar2 = fooBar2;
    }
}
