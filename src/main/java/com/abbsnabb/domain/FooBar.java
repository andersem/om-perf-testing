package com.abbsnabb.domain;

import java.util.List;

public class FooBar {
    public final String arbeidsgiver;
    public final String arbeidstaker;
    public final List<SomethingElse> somethingElse;

    public FooBar(String arbeidsgiver, String arbeidstaker, List<SomethingElse> somethingElse) {
        this.arbeidsgiver = arbeidsgiver;
        this.arbeidstaker = arbeidstaker;

        this.somethingElse = somethingElse;
    }
}
